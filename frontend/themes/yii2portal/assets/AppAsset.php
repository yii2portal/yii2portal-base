<?php


namespace frontend\themes\yii2portal\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $sourcePath = '@themePath/client';

    public $css = [
        'css/style.css',
    ];
    public $js = [
        'js/main.js',
//        'js/media.js',

        'js/swfobject.js',
        'js/jquery.cookie.js',

//        'http://jwpsrv.com/library/XOtnEl2IEeObLhIxOQfUww.js',
    ];
    public $depends = [
        'yii\web\JqueryAsset'
    ];

}
