<?php


namespace frontend\themes\yii2portal\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class FancyboxAsset extends AssetBundle
{
    public $sourcePath = '@themePath/client';

    public $css = [
        'js/fancybox/jquery.fancybox.css',
        'js/fancybox/helpers/jquery.fancybox-thumbs.css',
    ];
    public $js = [
        '//cdnjs.cloudflare.com/ajax/libs/fancybox/2.1.4/jquery.fancybox.pack.js',
        'js/fancybox/helpers/jquery.fancybox-thumbs.js',
        'js/fancylinks.js',
    ];
    public $depends = [
        'frontend\themes\yii2portal\assets\AppAsset'
    ];
    
    
}
