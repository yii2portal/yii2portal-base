<?php


namespace frontend\themes\yii2portal\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class PrintAsset extends AssetBundle
{
    public $sourcePath = '@themePath/client';

    public $css = [
        'css/print.css',
    ];

    public $cssOptions = [
        'media'=>'print'
    ];

    public $js = [];
    public $depends = [
        'frontend\themes\yii2portal\assets\AppAsset'
    ];
    
    
}
