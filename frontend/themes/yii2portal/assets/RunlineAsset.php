<?php


namespace frontend\themes\yii2portal\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class RunlineAsset extends AssetBundle
{
    public $sourcePath = '@themePath/client';


    public $js = [
        'js/runline.js',
    ];

    public $depends = [
        'frontend\themes\yii2portal\assets\AppAsset'
    ];
}
