$(document).ready(function () {

    $('.fontsize a').click(function () {
        var cl = $('main.content').attr('class');
        var re = /[\s]?size([\d]+)/ig;
        var out = re.exec(cl);
        var size = parseInt(out[1]);
        if ($(this).hasClass('sm')) {
            size--;
        } else if ($(this).hasClass('bg')) {
            size++;
        } else {
            size = 1;
        }

        if (size < 0 || size > 3) {
            return false;
        }
        $.cookie('font_size', size);
        $('main.content').attr('class', cl.replace(re, '')).addClass('size' + size);
        return false;
    });

    if ($.cookie('font_size')){
        var cl = $('main.content').attr('class');
        var re = /[\s]?size([\d]+)/ig;
        var size = $.cookie('font_size');
        $('main.content').attr('class', cl.replace(re, '')).addClass('size' + size);
    }

});