$(document).ready(function () {

    var timeout = 60;
    var timeout_line = 2000;
    //////
    var co = $('#runlinenews a').length;
    var line = 0;
    var pos = -1;
    var str = '';
    var strlen = '';

    function writeLine() {
        var clone = $('#runlinenews a:eq(' + line + ')').clone();
        str = clone.html();
        strlen = str.length;
        clone.html('');
        $('#theTicker').html('').append(clone);
        writeSymb(0);
    }

    function writeSymb() {
        setTimeout(function () {
            if (++pos == strlen) {
                pos = -1;
                if (++line == co) {
                    line = 0;
                }
                setTimeout(function () {
                    writeLine();
                }, timeout_line);
            } else {
                $('#theTicker a').html($('#theTicker a').html() + str.charAt(pos));
                writeSymb();
            }
        }, timeout);

    }

    writeLine();

});