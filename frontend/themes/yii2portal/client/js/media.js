$(document).ready(function() {


    $('[data-gallery-id]').click(function() {

        $.getJSON("/system/media/gallery/news_" + $(this).data('gallery-id'), {}, function(data) {
            console.log(data);
            $.fancybox.open(data, {
                tpl: {
                    closeBtn: '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>',
                    next: '<a title="Следующая фотография" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
                    prev: '<a title="Предыдущая фотография" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
                },
                autoResize: false,
                autoSize: false,
                fitToView: false,
                helpers: {
                    thumbs: {
                        width: 50,
                        height: 50
                    }
                }
            });
        });
        return false;
    });


    $('[data-video-id]').click(function() {


        $.getJSON("/system/media/video/news_" + $(this).data('video-id'), {}, function(data) {
            var videos = [];

            $.each(data, function(i, el) {
                var wrapId = '#localplayer_' + el.id + '_wrap';
                videos.push({
                    'href': wrapId,
                    'title': el.title,
                    'path': el.path,
                    'ext': el.ext
                });
                if ($(wrapId).length == 0) {

                    var wrapper = $("<div/>", {
                        'id': 'localplayer_' + el.id + '_wrap',
                        'css': {
                            'display': 'none'
                        }
                    }).appendTo('body');

                    $('<a/>', {
                        id: 'localplayer_' + el.id,
                        href: el.path,
                        css: {
                            width: 720,
                            height: 480,
                            'display': 'block'
                        }
                    }).appendTo(wrapper);

                    var playerData = {};
                    jwplayer('localplayer_' + el.id).setup({
                        file: el.path,
                        width: 720,
                        height: 480,
                        image: el.image,
                        title: el.title,
                        primary: "flash",
                        displaytitle: true
                    });
                }
            });

            $.fancybox.open(videos, {
                tpl: {
                    closeBtn: '<a title="Закрыть" class="fancybox-item fancybox-close" href="javascript:;"></a>',
                    next: '<a title="Следующее видео" style="height:90%;" class="fancybox-nav fancybox-next" href="javascript:;"><span></span></a>',
                    prev: '<a title="Предыдущее видео" style="height:90%;" class="fancybox-nav fancybox-prev" href="javascript:;"><span></span></a>'
                },
                autoResize: false,
                autoSize: false,
                fitToView: false,
                width: 720,
                height: 480,
                scrolling: "no",
                closeClick: false,
                openEffect: 'none',
                closeEffect: 'none'
            });



        });
        return false;
    });


});