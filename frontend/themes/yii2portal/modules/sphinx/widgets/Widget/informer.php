<?php

use \yii\widgets\ActiveForm;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model yii2portal\sphinx\models\SearchForm */

$structure = Yii::$app->getModule('structure');
$formatter = Yii::$app->formatter;
$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

?>


<div class="search">
    <?php
    $form = ActiveForm::begin([
        'id' => 'search',
        'method' => 'get',
        'action' => $structure->getPageByModule('sphinx')->urlPath
    ]);
    ?>
    <?php echo $form->field($model, 'text', [
        'options' => [
            'tag' => false
        ],
        'template' => '{input}'
    ])->textInput([
        'class' => 'inp',
        'placeholder' => 'Поиск по сайту'
    ]) ?>
    <?php echo Html::submitButton('OK', ['class' => 'button']) ?>
    <?php ActiveForm::end() ?>
</div>