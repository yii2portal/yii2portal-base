<?php

use \yii\widgets\LinkPager;
use \yii\helpers\Html;
use \yii\widgets\ActiveForm;
use \yii\helpers\ArrayHelper;

/* @var $this yii\web\View */
/* @var $provider yii\sphinx\ActiveDataProvider */
/* @var $page \yii2portal\structure\models\CoreStructure */
/* @var $news \yii2portal\news\models\News[] */
/* @var $main \yii2portal\news\models\News */
/* @var $other_main \yii2portal\news\models\News[] */
/* @var $defaultMainImage \yii2portal\media\models\Media */

//$this->title = $page->title;

$news = $provider->getModels();

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');
$this->registerCssFile($bundle->baseUrl . "/css/search.css");
$this->registerCssFile($bundle->baseUrl . "/js/select2/select2.css");
$this->registerJsFile($bundle->baseUrl . "/js/select2/select2.min.js", ['depends' => ['yii\web\JqueryAsset']]);
$this->registerJsFile($bundle->baseUrl . "/js/select2/select2_locale_ru.js", ['depends' => ['yii\web\JqueryAsset']]);
$this->registerJsFile($bundle->baseUrl . "/js/search.js", ['depends' => ['yii\web\JqueryAsset']]);


$newsCategories = Yii::$app->getModule('structure')->getPagesByModule('news');

?>


<div class="searchform">
    <?php



    $form = ActiveForm::begin([
        'id' => 'megaform',
        'action' => $page->urlPath,
        'method' => 'get',
        'options' => ['class' => 'srhform'],
        'fieldConfig' => [
            'options' => [
                'tag' => false
            ],
            'template' => "{label}\n <div>{input}</div>",
            'inputOptions' => [
                'class' => 'inp'
            ],
        ]
    ]);
    ?>
    <div align="center">
        <table cellpadding="0" cellspacing="2" width="100%">
            <tr style="vertical-align: top;">
                <td style="width: 54%">
                    <?php echo $form->field($model, 'text')->textInput() ?>

                    <?php echo $form->field($model, 'category', [

                    ])->dropDownList(ArrayHelper::map($newsCategories, 'id', 'title'), [
                        'multiple' => 'multiple',
                        'id' => 'selcat'
                    ]) ?>
                </td>
                <td style="width: 46%">
                    <?php echo $form->field($model, 'author')->widget(\yii\jui\AutoComplete::classname(), [
                        'options'=>[
                            'class'=>'inp'
                        ],
                        'clientOptions' => [
                            'minChars' => 2,
                            'maxHeight' => 400,
                            'width' => 300,
                            'zIndex' => 9999,
                            'deferRequestBy' => 0,
                            'source'=>"{$page->urlPath}autocomplete/"
                        ],
                    ]) ?>


                    <label>Сортировать результаты</label>

                    <div>
                        <?php echo $form->field($model, 'sortby', [
                            'template'=>'{input}'
                        ])->dropDownList($model->orders(), [
                            'class'=>''
                        ]) ?>
                        <?php echo $form->field($model, 'direction', [
                            'template'=>'{input}'
                        ])->dropDownList([SORT_DESC=>"По убыванию", SORT_ASC=>'По возрастанию'], [
'class'=>''
                        ]) ?>


                    </div>

                </td>
            </tr>
            <tr>
                <td class="search">
                    <div style="margin-top:6px">
                        <?php echo Html::submitButton('Начать поиск', ['class' => 'button']) ?>
                        <?php echo Html::resetButton('Сбросить', ['class' => 'clear']) ?>
                    </div>
                </td>
                <td class="search">

                </td>
            </tr>
        </table>
        <br/>
    </div>
    <?php ActiveForm::end() ?>
</div>


<div class="lenta_news">
    <?php foreach ($news as $i => $indexNew): ?>
        <?php
        $new = $indexNew->object;
        if(!$new){
            continue;
        }
        ?>
        <div class="one_news">
            <div class='clearfix'>
                <?php if ($new->imageLenta): ?>
                    <a href="<?php echo $new->urlPath ?>">
                        <?php
                        echo Html::img($new->imageLenta->thumbnail(80, 60, $bundle->baseUrl . '/ima/nophoto.png'), [
                            'alt' => $new->title,
                            'class' => 'fl'
                        ]);
                        ?>
                    </a>
                <?php endif; ?>

                <div>
                    <span class="date"><?php echo $new->dateTimeFormat("dd/MM/yyyy HH:mm"); ?></span>
                    <a href="<?php echo $new->urlPath ?>" class="n"><?php echo $new->title ?></a>
                    <?php if ($new->hasPhoto): ?>
                        <a href="<?php echo $new->urlPath ?>" title="фото" class="has_photo">фото</a>
                    <?php endif; ?>

                    <?php if ($new->hasVideo): ?>
                        <a href="<?php echo $new->urlPath ?>" title="видео" class="has_video">видео</a>
                    <?php endif; ?>

                    <?php if ($new->hasInfo): ?>
                        <a href="<?php echo $new->urlPath ?>" title="инфографика" class="has_info">инфографика</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    <?php endforeach; ?>
</div>

<?php
echo LinkPager::widget([
    'pagination' => $provider->pagination,
    'registerLinkTags' => true,

]);
?>
