<?php

/* @var $this yii\web\View */
/* @var $data \yii2portal\news\models\News[] */

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

?>


<?php if(count($data)>0):?>
<div class="column_title">НОВОСТИ РАЗДЕЛА</div>
<div class="lenta_news">
    <?php foreach ($data as $new): ?>
    <div class="one_news">
        <div>
            <span class="date"><?php echo $new->dateTimeFormat("dd/MM/yyyy HH:mm"); ?></span>
            <a class="n" href="<?php echo $new->urlPath;?>"><?php echo $new->title;?></a>
        </div>
    </div>
    <?php endforeach; ?>
</div>
<?php endif;?>