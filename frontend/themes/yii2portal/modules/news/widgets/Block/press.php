<?php

use \yii2portal\media\models\Media;

/* @var $this yii\web\View */
/* @var $data \yii2portal\news\models\News[] */


$formatter = Yii::$app->formatter;

$press = array();
foreach ($data as $new) {
    $key = strtotime(date('j-n-Y', $new->press_time));
    if (!isset($press[$key])) {
        $press[$key] = [];
    }
    $press[$key][] = $new;
}

$newsPressPage = Yii::$app->getModule('structure')->getPageByModule('news-press');

$defaultImage = null;
if ($newsPressPage) {
    $defaultImage = Media::findOne($newsPressPage->getParamValue('default_image'));
}

?>

<?php if (count($data) > 0): ?>
    <div class="news_column_list press">
        <?php foreach ($press as $day): ?>
            <?php if (count($day) > 1): ?>
                <h3>Пресс-конференции <?php $formatter->dateFormat($day,"d MMMM")?></h3>
            <?php else: ?>
                <h3>Пресс-конференция <?php $formatter->dateFormat($day,"d MMMM")?></h3>
            <?php endif; ?>
            <?php foreach ($day as $i=>$new): ?>
                <div class="block">
                    <?php if($i == 0):?>
                        <?php if ($new->imageCol): ?>
                            <a href="<?php echo $new->urlPath ?>">
                                <?php
                                echo Html::img($new->imageCol->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                                    'alt' => $new->title
                                ]);
                                ?>
                            </a>
                        <?php elseif ($defaultImage): ?>
                            <a href="<?php echo $new->urlPath ?>">
                                <?php
                                echo Html::img($defaultImage->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                                    'alt' => $new->title
                                ]);
                                ?>
                            </a>
                        <?php endif; ?>
                    <?php endif;?>
                    <a href="<?php echo $new->urlPath ?>" class="title"><span><?php echo $new->dateTimeFormat("HH:mm", 'press_time'); ?></span><?php echo $new->title ?></a>
                </div>
            <?php endforeach; ?>
        <?php endforeach; ?>
    </div>
<?php endif; ?>