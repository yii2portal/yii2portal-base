<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data \yii2portal\news\models\News[] */

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

?>

<?php foreach ($data as $i => $new): ?>
    <?php if ($i == 0): ?>
        <h3><?php echo $new->parent->title ?></h3>
    <?php endif; ?>
    <div class="block simple">
        <?php if ($new->imageCol): ?>
            <?php
            echo Html::img($new->imageCol->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                'alt' => $new->title
            ]);
            ?>
        <?php endif; ?>
        <span class="red">«</span><?php echo nl2br($new->title) ?><span class="red">»</span>.
        <p class="doslovno_sign"><?php echo nl2br($new->description) ?></p>
    </div>
<?php endforeach; ?>