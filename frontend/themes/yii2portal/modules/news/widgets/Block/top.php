<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data \yii2portal\news\models\News[] */

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

$news = $data;
shuffle($news);
if (count($news) > 2) {
    $news = array_slice($news, 0, 2);
}
?>

<?php if (count($news) > 0): ?>
    <div class="news">
        <?php foreach ($news as $new): ?>
            <div>
                <?php if ($new->imageCol): ?>
                    <a href="<?php echo $new->urlPath ?>">
                        <?php
                        echo Html::img($new->imageCol->thumbnail(72, 54, $bundle->baseUrl . '/ima/nophoto.png'), [
                            'alt' => $new->title
                        ]);
                        ?>
                    </a>
                <?php endif; ?>
                <a href="<?php echo $new->urlPath ?>" class="black_link"
                   title="<?php echo $new->title ?>"><?php echo $new->title ?></a>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>