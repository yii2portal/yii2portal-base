<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data \yii2portal\news\models\News[] */

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

?>

<?php foreach ($data as $i => $new): ?>
    <?php if ($i == 0): ?>
        <h3><a href="<?php echo $new->parent->urlPath ?>"><?php echo $new->parent->title ?></a></h3>
    <?php endif; ?>
    <div class="block simple">
        <?php if ($new->imageCol): ?>
            <?php
            echo Html::img($new->imageCol->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                'alt' => $new->title
            ]);
            ?>
        <?php endif; ?>
        <a href="<?php echo $new->urlPath ?>" class="title"><?php echo $new->title ?></a>
        <p class="ta_r"><a class="more" href="<?php echo $new->urlPath ?>">Подробнее</a></p>
    </div>
<?php endforeach; ?>