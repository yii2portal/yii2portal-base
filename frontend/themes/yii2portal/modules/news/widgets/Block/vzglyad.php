<?php

use \yii2portal\media\models\Media;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data \yii2portal\news\models\News[] */

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

$newsVzglyadPage = Yii::$app->getModule('structure')->getPageByModule('news-vzglyad');

$defaultImage = null;
if ($newsVzglyadPage) {
    $defaultImage = Media::findOne($newsVzglyadPage->getParamValue('default_image'));
}

?>

<div class="news_column_list">
    <?php foreach ($data as $new): ?>

        <h3><a href="<?php echo $new->parent->urlPath ?>"><?php echo $new->parent->title ?></a></h3>
        <div class="block">
            <?php if ($new->imageCol): ?>
                <a href="<?php echo $new->urlPath ?>">
                    <?php
                    echo Html::img($new->imageCol->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                        'alt' => $new->title
                    ]);
                    ?>
                </a>
            <?php elseif ($defaultImage): ?>
                <a href="<?php echo $new->urlPath ?>">
                    <?php
                    echo Html::img($defaultImage->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                        'alt' => $new->title
                    ]);
                    ?>
                </a>
            <?php endif; ?>

            <a href="<?php echo $new->urlPath ?>" class="title"><?php echo $new->title ?></a>
            <?php if ($new->hasPhoto): ?>
                <a href="<?php echo $new->urlPath ?>" title="фото" class="has_photo">фото</a>
            <?php endif; ?>

            <?php if ($new->hasVideo): ?>
                <a href="<?php echo $new->urlPath ?>" title="видео" class="has_video">видео</a>
            <?php endif; ?>

            <?php if ($new->hasInfo): ?>
                <a href="<?php echo $new->urlPath ?>" title="инфографика" class="has_info">инфографика</a>
            <?php endif; ?>

            <p class="ta_r"><a class="more" href="<?php echo $new->urlPath ?>">Подробнее</a></p>
        </div>
    <?php endforeach; ?>
</div>