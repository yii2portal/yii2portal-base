<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $data \yii2portal\news\models\News[] */

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');
?>

<div class="news_column_list">
    <?php foreach ($data as $new): ?>
        <div class="block">
            <h3><a href="<?php echo $new->parent->urlPath ?>"><?php echo $new->parent->title ?></a></h3>

            <?php if ($new->imageCol): ?>
                <a href="<?php echo $new->urlPath ?>">
                    <?php
                    echo Html::img($new->imageCol->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                        'alt' => $new->title
                    ]);
                    ?>
                </a>
            <?php endif; ?>

            <a href="<?php echo $new->urlPath ?>" class="title"><?php echo $new->title ?></a>
            <?php if ($new->hasPhoto): ?>
                <a href="<?php echo $new->urlPath ?>" title="фото" class="has_photo">фото</a>
            <?php endif; ?>

            <?php if ($new->hasVideo): ?>
                <a href="<?php echo $new->urlPath ?>" title="видео" class="has_video">видео</a>
            <?php endif; ?>

            <?php if ($new->hasInfo): ?>
                <a href="<?php echo $new->urlPath ?>" title="инфографика" class="has_info">инфографика</a>
            <?php endif; ?>

            <p class="ta_r"><a class="more" href="<?php echo $new->urlPath ?>">Подробнее</a></p>
        </div>
    <?php endforeach; ?>
</div>