<?php

/* @var $this yii\web\View */
/* @var $data \yii2portal\news\models\News[] */



$structure = Yii::$app->getModule('structure');
$index = $structure->getPageByModule('index');
$runlinelimit = $index->getParamValue('runlinelimit', 5);

if (count($data) > $runlinelimit) {
    shuffle($data);
    $data = array_slice($data, 0, $runlinelimit);
}

?>

<div id="runlinenews" style="display: none;">
    <?php foreach ($data as $new): ?>
        <a href="<?php echo $new->urlPath ?>"><?php echo $new->title ?></a>
    <?php endforeach; ?>
</div>