<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $news \yii2portal\news\models\News[] */
/* @var $params [] */

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');
$this->registerCssFile($bundle->baseUrl . "/css/similar.css");
?>

<div class="cpluginSimilarWrapper" style="width:<?php echo $params['width']; ?>;<?php echo $params['style'] ?>">
    <div class="cpluginSimilar news_column_list">
        <?php foreach ($news as $new): ?>
            <div class="block">
                <?php if ($new->imageCol): ?>
                    <a href="<?php echo $new->urlPath ?>">
                        <?php
                        echo Html::img($new->imageCol->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                            'alt' => $new->title
                        ]);
                        ?>
                    </a><br/>
                <?php endif; ?>
                <a href="<?php echo $new->urlPath ?>" class="title"><?php echo $new->title ?></a>

                <p class="ta_r"><a class="more" href="<?php echo $new->urlPath ?>">Подробнее</a></p>
            </div>
        <?php endforeach; ?>
    </div>
</div>
