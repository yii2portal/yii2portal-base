<?php

use \yii\widgets\LinkPager;
use  \yii\helpers\Html;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $category \yii2portal\structure\models\CoreStructure */
/* @var $new \yii2portal\news\models\News */

$social = Yii::$app->getModule('social');
$this->params['breadcrumbs'][] = ['label' => strip_tags($new->title), 'url' => $new->urlPath];

?>

<div>
    <div class="news_info">
        <h2><?php echo strip_tags($new->title) ?></h2>
    </div>
    <div class="cont">
        <h3>Время проведения: <?php echo $new->dateTimeFormat("d MMMM, HH:mm", 'press_time')?>{{$new.press_time|date:"j F, H:i"}}</h3>
        <?php echo $new->content ?>
    </div>
    <p class="print"><a href="javascript:window.print();">версия для печати</a></p>
    <?php echo $social->informer->insert('informer', $new->title, $new->urlPath, $new->id); ?>
</div>