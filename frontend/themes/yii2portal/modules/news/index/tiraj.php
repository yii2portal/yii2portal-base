<?php

use \yii\widgets\LinkPager;
use  \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $provider \yii\data\ActiveDataProvider */
/* @var $page \yii2portal\structure\models\CoreStructure */
/* @var $news \yii2portal\news\models\News[] */
/* @var $main \yii2portal\news\models\News */
/* @var $other_main \yii2portal\news\models\News[] */
/* @var $defaultMainImage \yii2portal\media\models\Media */

//$this->title = $page->title;

$openx = Yii::$app->getModule('openx');

$news = $provider->getModels();

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

?>


<?php if ($main): ?>
    <div class="main_news clearfix">
        <div class="pic">


            <a href="<?php echo $new->urlPath ?>">
                <?php if ($main->imageMain): ?>
                    <?php
                    echo Html::img($main->imageMain->thumbnail(215, 161, $bundle->baseUrl . '/ima/nophoto.png'), [
                        'alt' => $main->title,
                        'class' => 'fl'
                    ]);
                    ?>
                <?php elseif ($defaultMainImage): ?>

                    <?php
                    echo Html::img($defaultMainImage->thumbnail(215, 161, $bundle->baseUrl . '/ima/nophoto.png'), [
                        'alt' => $main->title,
                        'class' => 'fl'
                    ]);
                    ?>
                <?php endif; ?>
            </a>


            <div></div>
        </div>
        <div class="text">
            <h2><a href="<?php echo $main->urlPath ?>"><?php echo $main->title ?></a></h2>
            <span class="date"><?php echo $main->dateTimeFormat("dd/MM/yyyy HH:mm"); ?></span>

            <div class="brief">

                <?php if (mb_strlen($main->description) <= 10): ?>
                    <?php if (preg_match('#(<p[^>]*>.*?</p>)#uim', $main->content, $out)): ?>
                        <?php echo strip_tags($out[0]) ?>
                    <?php endif; ?>
                <?php else: ?>
                    <?php echo $main->description ?>
                <?php endif; ?>


                <?php if ($main->hasPhoto): ?>
                    <a href="<?php echo $main->urlPath ?>" title="фото" class="has_photo">фото</a>
                <?php endif; ?>
                <?php if ($main->hasVideo): ?>
                    <a href="<?php echo $main->urlPath ?>" title="видео" class="has_video">видео</a>
                <?php endif; ?>
                <?php if ($main->hasInfo): ?>
                    <a href="<?php echo $main->urlPath ?>" title="инфографика" class="has_info">инфографика</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (count($other_main > 0)): ?>
    <div class="main_last_news">
        <?php foreach ($other_main as $i => $new): ?>
            <div class="one_news">
                <span class="date"><?php echo $new->dateTimeFormat("dd/MM/yyyy HH:mm"); ?></span>
                <div>
                    <p>
                        <a href="<?php echo $new->urlPath ?>"><?php echo $new->title ?></a>
                        <?php if ($new->hasPhoto): ?>
                            <a href="<?php echo $new->urlPath ?>" title="фото" class="has_photo">фото</a>
                        <?php endif; ?>
                        <?php if ($new->hasVideo): ?>
                            <a href="<?php echo $new->urlPath ?>" title="видео" class="has_video">видео</a>
                        <?php endif; ?>
                        <?php if ($new->hasInfo): ?>
                            <a href="<?php echo $new->urlPath ?>" title="инфографика"
                               class="has_info">инфографика</a>
                        <?php endif; ?>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>
<?php echo $openx->placeholder->insert(6); ?>
<?php if ($main || count($other_main > 0)): ?>
    <div class="column_title" style="margin-top: 15px;"></div>
<?php endif; ?>


<div class="lenta_tiraj">
    <?php foreach ($news as $i => $new): ?>
        <div class="block simple">
            <?php if ($new->newsAgency && $new->newsAgency->imageLogo): ?>
                <?php
                echo Html::img($new->newsAgency->imageLogo->thumbnail(215, 161, $bundle->baseUrl . '/ima/nophoto.png'), [
                    'alt' => $new->title,
                    'class' => 'fl'
                ]);
                ?>
            <?php endif; ?>
            <a href="<?php echo $new->urlPath ?>" class="black_link"><?php echo $new->title ?></a>

            <?php if ($new->hasPhoto): ?>
                <a href="<?php echo $new->urlPath ?>" title="фото" class="has_photo">фото</a>
            <?php endif; ?>

            <?php if ($new->hasVideo): ?>
                <a href="<?php echo $new->urlPath ?>" title="видео" class="has_video">видео</a>
            <?php endif; ?>

            <?php if ($new->hasInfo): ?>
                <a href="<?php echo $new->urlPath ?>" title="инфографика" class="has_info">инфографика</a>
            <?php endif; ?>

            <p class="ta_r"><a class="more" href="<?php echo $new->urlPath ?>">Подробнее</a></p>
        </div>
    <?php endforeach; ?>
</div>

<?php
echo LinkPager::widget([
    'pagination' => $provider->pagination,
    'registerLinkTags' => true,

]);
?>
