<?php

use \yii\helpers\ArrayHelper;
use  \yii\helpers\Html;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $provider \yii\data\ActiveDataProvider */
/* @var $category \yii2portal\structure\models\CoreStructure */
/* @var $new \yii2portal\news\models\News */

$openx = Yii::$app->getModule('openx');
$social = Yii::$app->getModule('social');
$sphinx = Yii::$app->getModule('sphinx');
$cplugin = Yii::$app->getModule('cplugin');

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

$news = Yii::$app->getModule('news');
$blockNews = $news->block;
$indexPage = Yii::$app->getModule('structure')->getPageByModule('index');



$sidelimit = $indexPage->getParamValue('sidelimit', 10);
$similarlimit = $indexPage->getParamValue('likelimit', 10);

$this->params['breadcrumbs'][] = ['label' => strip_tags($new->title), 'url' => $new->urlPath];

$news->meta->register($new);

?>

<div>

    <div class="left_inner_column">
        <?php if ($new->video): ?>
            <div class="video">
                <?php echo $new->video->widget->getCode('215', '140'); ?>
                <div></div>
            </div>
        <?php elseif ($new->baseImage): ?>
            <div class="pic">
                <?php
                echo Html::img($new->baseImage->thumbnail(215, 161, $bundle->baseUrl . '/ima/nophoto.png'), [
                    'alt' => $new->title,
                    'title' => $new->baseImage->description,
                ]);
                ?>

                <div></div>
                <?php if (!empty($new->baseImage->source)): ?>
                    <p>
                        Фото
                        <?php if ($new->baseImage->sourceUrl): ?>
                            <?php echo Html::a($new->baseImage->source, $new->baseImage->sourceUrl) ?>
                        <?php else: ?>
                            <?php echo $new->baseImage->source ?>
                        <?php endif; ?>
                    </p>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php echo $openx->placeholder->insert(20); ?>

        <?php echo $blockNews->insert('catnews')
            ->andWhere(['pid' => $category->id])
            ->andWhere(['not', ['id' => $new->id]])
            ->andPublished()
            ->byDatepublic()
            ->orderBy(['datepublic' => SORT_DESC])
            ->limit($sidelimit)
            ->all(); ?>


    </div>
    <div class="right_inner_column">
        <div class="news_info">
            <h2><?php echo strip_tags($new->title) ?></h2>
            <span class="date">
            <?php
            $metaData = [];

            $metaData[] = $new->dateTimeFormat("dd/MM/yyyy HH:mm");
            if ($new->newsCity) {
                $metaData[] = $new->newsCity->title . ($new->newsAgency ? " - {$new->newsAgency->title}" : '');
            } elseif ($new->newsAgency) {
                $metaData[] = $new->newsAgency->title;
            }

            foreach ($new->allAuthors as $author) {
                $metaData[] = "<span>{$author}</span>";
            }

            echo implode(', ', $metaData);
            ?>


        </span>
        </div>
        <div class="cont">
            <?php if ($new->baseImage): ?>
                <?php
                echo Html::img($new->baseImage->thumbnail(215, 161, $bundle->baseUrl . '/ima/nophoto.png'), [
                    'alt' => $new->title,
                    'class' => 'print_img'
                ]);
                ?>
            <?php endif; ?>

            <?php echo $cplugin->render($new->content) ?>


            <?php if (!empty($new->storify)): ?>
                <div class="storify">
                    <iframe src="//storify.com/<?php echo $new->storify ?>/embed?border=false" width="100%" height="750"
                            frameborder="no" allowtransparency="true"></iframe>

                    <?php $this->registerJsFile("//storify.com/{$new->storify}.js?border=false&css=http://24.kg/styles/user/style.css"); ?>
                    <noscript>[<a href="//storify.com/<?php echo $new->storify ?>" target="_blank">View the story
                            "Getting started" on Storify</a>]
                    </noscript>
                </div>
            <?php endif; ?>
        </div>
        <?php if ($new->show_gal && $new->galleryItems): ?>
            <?php \frontend\themes\yii2portal\assets\FancyboxAsset::register($this); ?>
            <div class="fotolist clearfix">
                <div class="column_title">Фотогалерея</div>
                <?php foreach ($new->galleryItems as $item): ?>

                    <a href="<?php echo $item->srcUrl ?>" class="fancybox"
                       title="<?php echo Html::encode($item->description); ?>" rel="gal">
                        <?php
                        echo Html::img($item->thumbnail(96, 90, $bundle->baseUrl . '/ima/nophoto.png'), [
                            'alt' => $item->description,
                            'title' => $item->description,
                        ]);
                        ?>
                    </a>

                <?php endforeach; ?>
            </div>
        <?php endif; ?>


        <?php if ($new->videos): ?>
            <div class="videolist clearfix">
                <div class="column_title">Видеогалерея</div>
                <?php foreach ($new->videos as $video): ?>
                    <div class='one'>
                        <?php echo $video->widget->getCode('430', '300'); ?>
                    </div>
                <?php endforeach; ?>
            </div>
        <?php endif; ?>

        <p class="url">URL:
            <a href="<?php echo $new->urlPath ?>"><?php echo Url::to($new->shortUrlPath, true); ?></a>
        </p>

        <p class="print"><a href="javascript:window.print();">версия для печати</a></p>
        <?php echo $social->informer->insert('informer', $new->title, $new->urlPath, $new->id); ?>


        <?php echo $openx->placeholder->insert(21); ?>


        <?php
        if ($this->beginCache('news-view-similar', ['variations' => [$new->id]])) {
            if (!empty($new->tagIds)) {
                echo $sphinx->similar->insert(implode(' ', ArrayHelper::getColumn($new->newsTags, 'title')), 'similar', $new->id, $similarlimit);
            } else {
                echo $sphinx->similar->insert($new->title, 'similar', $new->id, $similarlimit, true);
            }

            $this->endCache();
        }
        ?>
        <div class="noprint">
            <?php echo $openx->placeholder->insert(24); ?>
        </div>
    </div>


</div>