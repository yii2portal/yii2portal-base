<?php

use \yii\widgets\LinkPager;
use  \yii\helpers\Html;
use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $category \yii2portal\structure\models\CoreStructure */
/* @var $new \yii2portal\news\models\News */

$social = Yii::$app->getModule('social');

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

$this->params['breadcrumbs'][] = ['label' => strip_tags($new->title), 'url' => $new->urlPath];

?>

<div>
    <?php if($new->newsAgency && $new->newsAgency->imageLogo):?>
        <p style="margin-bottom: 12px;">
            <?php
            echo Html::img($new->newsAgency->imageLogo->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                'alt' => $new->title
            ]);
            ?>
        </p>
    <?php endif;?>
    <div class="news_info">
        <h2><?php echo strip_tags($new->title) ?></h2>
    </div>
    <div class="cont">
        <?php echo $new->content ?>


        <?php if (!empty($new->storify)): ?>
            <div class="storify">
                <iframe src="//storify.com/<?php echo $new->storify ?>/embed?border=false" width="100%" height="750"
                        frameborder="no" allowtransparency="true"></iframe>

                <?php $this->registerJsFile("//storify.com/{$new->storify}.js?border=false&css=http://24.kg/styles/user/style.css");?>
                <noscript>[<a href="//storify.com/<?php echo $new->storify ?>" target="_blank">View the story
                        "Getting started" on Storify</a>]
                </noscript>
            </div>
        <?php endif; ?>

    </div>
    <p class="print"><a href="javascript:window.print();">версия для печати</a></p>


    <?php echo $social->informer->insert('informer', $new->title, $new->urlPath, $new->id); ?>
</div>