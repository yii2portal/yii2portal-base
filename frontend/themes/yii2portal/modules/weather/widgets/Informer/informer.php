<?php


/* @var $this yii\web\View */
/* @var $items \yii2portal\weather\models\Weather[] */

$formatter = Yii::$app->formatter;
$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');
$icon = (date('H') >= 9 && date('H') <= 5 ? 'night' : 'day');
?>


<div class="block weather_block">
    <h3>Прогноз погоды</h3>
    <div class="grey_zag"><?php echo $formatter->asDate($items[0]->dateline + $timeShift, "dd.MM.yyyy"); ?></div>
    <table>
        <tr>
            <th class='weather_z'>&nbsp;</th>
            <th class='weather_n'>ночь</th>
            <th class='weather_d'>день</th>
        </tr>
        <?php foreach ($items as $i => $item): ?>
            <?php
            $weatherItem = $weather[$item->id];
            $isLast = $i + 1 == count($items);
            ?>
            <tr>
                <td colspan='3' class='weather_z'><?php echo $item->title ?></td>
            </tr>
            <tr>
                <td class='weather_pix'><img
                        src='<?php echo $bundle->baseUrl ?>/ima/weather/<?php echo $weatherItem->{$icon}['icon'] ?>.gif'
                        alt="" title=""/></td>
                <td class='weather_n'>
                    <?php if ($weatherItem->night['tmin'] > 0): ?>+<?php endif; ?><?php echo $weatherItem->night['tmin']; ?>...<?php if ($weatherItem->night['tmax'] > 0): ?>+<?php endif; ?><?php echo $weatherItem->night['tmax']; ?>
                </td>
                <td class='weather_d'>
                    <?php if ($weatherItem->day['tmin'] > 0): ?>+<?php endif; ?><?php echo $weatherItem->day['tmin']; ?>...<?php if ($weatherItem->day['tmax'] > 0): ?>+<?php endif; ?><?php echo $weatherItem->day['tmax']; ?>
                </td>
            </tr>
            <?php if (!$isLast): ?>
                <tr>
                    <td colspan='3'>
                        <div class='weather_sep'></div>
                    </td>
                </tr>
            <?php endif; ?>
        <?php endforeach; ?>
    </table>
</div>