<?php

use \yii2portal\archive\assets\CalendarAsset;

/* @var $this yii\web\View */

$structure = Yii::$app->getModule('structure');
$formatter = Yii::$app->formatter;
$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

CalendarAsset::register($this);

?>

<div id="calendar_cont" data-dateline="<?php echo date('Y/m/d')?>"></div>