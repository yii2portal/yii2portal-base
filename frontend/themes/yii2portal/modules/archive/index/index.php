<?php

use \yii\widgets\LinkPager;
use  \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $provider \yii\data\ActiveDataProvider */
/* @var $page \yii2portal\structure\models\CoreStructure */
/* @var $news \yii2portal\news\models\News[] */
/* @var $main \yii2portal\news\models\News */
/* @var $other_main \yii2portal\news\models\News[] */
/* @var $defaultMainImage \yii2portal\media\models\Media */

//$this->title = $page->title;

$openx = Yii::$app->getModule('openx');

$news = $provider->getModels();

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');
$formater = Yii::$app->formatter;
$url = Yii::$app->getModule('structure')->getPageByModule('archive')->urlPath;

$this->params['breadcrumbs'][] = ['label' => $formater->asDate(mktime(0,0,0, $month, $day, $year), 'dd MMMM yyyy года'),
    'url' => $url. date('Y/m/d', mktime(0, 0, 0, $month, $day, $year))];

?>

<div class="lenta_news">
    <?php foreach ($news as $i => $new): ?>
        <div class="one_news">
            <div class='clearfix'>
                <?php if ($new->imageLenta): ?>
                    <a href="<?php echo $new->urlPath ?>">
                        <?php
                        echo Html::img($new->imageLenta->thumbnail(80, 60, $bundle->baseUrl . '/ima/nophoto.png'), [
                            'alt' => $new->title,
                            'class' => 'fl'
                        ]);
                        ?>
                    </a>
                <?php endif; ?>

                <div>
                    <span class="date"><?php echo $new->dateTimeFormat("dd/MM/yyyy HH:mm"); ?></span>
                    <a href="<?php echo $new->urlPath ?>" class="n"><?php echo $new->title ?></a>
                    <?php if ($new->hasPhoto): ?>
                        <a href="<?php echo $new->urlPath ?>" title="фото" class="has_photo">фото</a>
                    <?php endif; ?>

                    <?php if ($new->hasVideo): ?>
                        <a href="<?php echo $new->urlPath ?>" title="видео" class="has_video">видео</a>
                    <?php endif; ?>

                    <?php if ($new->hasInfo): ?>
                        <a href="<?php echo $new->urlPath ?>" title="инфографика" class="has_info">инфографика</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if ($i == 6): ?><?php echo $openx->placeholder->insert(31); ?><?php endif; ?>
    <?php endforeach; ?>
</div>


