<?php

use \yii\helpers\Html;

/* @var $this yii\web\View */
$social = Yii::$app->getModule('social');
$formatter = Yii::$app->formatter;
$url = Yii::$app->getModule('structure')->getPageByModule('archive')->urlPath;
?>
<div class="block archive">
    <h3><a href="<?php echo $url ?>">АРХИВ</a></h3>

    <div id="calendar-layer">
        <table class="calendar">
            <tr>
                <th width="10"><a href="<?php echo $url. date('Y/m/d', mktime(0, 0, 0, $curmonth, $curday, $curyear-1))?>" class="prevyear" title="Предыдущий год">&laquo;&laquo;</a>
                </th>
                <th width="5"><a href="<?php echo $url. date('Y/m/d', mktime(0, 0, 0, $curmonth-1, $curday, $curyear))?>" class="prev" title="Предыдущий месяц">&laquo;</a></th>
                <th width="120"><?php echo $formatter->asDate(mktime(0, 0, 0, $curmonth, 1, $curyear), 'LLLL yyyy')?></th>
                <th width="5"><a href="<?php echo $url. date('Y/m/d', mktime(0, 0, 0, $curmonth+1, $curday, $curyear))?>" class="next" title="Следующий месяц">&raquo;</a></th>
                <th width="10"><a href="<?php echo $url. date('Y/m/d', mktime(0, 0, 0, $curmonth, $curday, $curyear+1))?>" class="nextyear" title="Следующий год">&raquo;&raquo;</a>
                </th>
        </table>
        <table class="calendar">
            <?php
            for($i=0;$i<count($calendar['weeks'])/7;$i++) {
                echo Html::tag('tr', implode("\n",array_slice($calendar['weeks'],$i*7, 7)));
            }
            ?>

</table>
    </div>
</div>