<?php


/* @var $this yii\web\View */
/* @var $polls \yii2portal\poll\models\Poll[] */

$structure = Yii::$app->getModule('structure');
$formatter = Yii::$app->formatter;
$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');
$poll = !empty($polls) ? array_shift($polls) : null;
?>

<?php if ($poll): ?>
    <div class="block">
        <h3><a href="<?php echo $structure->getPageByModule('poll')->urlPath ?>">Голосование</a></h3>
        <div class="poll poll_list">
            <a href="<?php echo $poll->urlPath ?>" class="title"><?php echo $poll->question ?></a>
            <?php if ($poll->type_cr == 'checkbox'): ?>
                <br/>
                <span class="grey_zag">от <?php echo $poll->count_min ?> до <?php echo $poll->count_max ?>
                    ответов</span>
            <?php endif; ?>

            <?php if ($poll->isActive): ?>
            <form action="<?php echo $poll->urlPath ?>" method="post" id="poll_form_main">
                <ul class="reset">
                    <?php foreach($poll->answers as $i=>$answer):?>
                    <li>
                        <div class='box'><input type="<?php echo $poll->type_cr ?>" value="<?php echo $answer['id']; ?>" name="el[]"
                                                id="poll_<?php echo $i?>"/></div>
                        <label for="poll_<?php echo $i?>" class="right"><?php echo $answer['title']; ?></label>
                    </li>
                    <?php endforeach;?>
                </ul>
                <p><input type="submit" value="Голосовать" name="ok"/></p>
            </form>
            <?php else:?>
            <ul class="pollresult">
                <?php foreach($poll->answers as $answer):?>
                <li>
                    <b><?php echo $answer['title']; ?></b>
                    <div class="fr"><span>(<?php echo $answer['count']; ?>)</span><span><?php echo $answer['proc']; ?>%</span></div>
                    <div class="totalVotes clear">
                        <div style="width:<?php echo $answer['proc']; ?>%;"></div>
                    </div>
                </li>
                <?php endforeach;?>
            </ul>
            <?php endif;?>
        </div>
    </div>
<?php endif; ?>

