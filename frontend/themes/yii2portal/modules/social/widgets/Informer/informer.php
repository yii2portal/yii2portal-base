<?php

use \yii\helpers\Url;
use \yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $items \yii2portal\weather\models\Weather[] */

?>

<noindex>
    <?php
        $this->registerJsFile('http://platform.twitter.com/widgets.js');
        $this->registerJsFile('https://apis.google.com/js/plusone.js');
        $this->registerJsFile('http://vk.com/js/api/openapi.js?49');

        $this->registerJs("VK.init({apiId: 5005297, onlyWidgets: true});", \yii\web\View::POS_END);

    $url = Url::to($link, true);
    $title = Html::encode(strip_tags($title));
    $vkJs = <<<EOF
VK.Widgets.Like("vk_like", {
                type: "mini",
                pageTitle:"{$title}",
                pageUrl:"{$url}",
            }, {$id});
EOF;

        $this->registerJs($vkJs, \yii\web\View::POS_END);
    ?>

    <div class="share noprint">
        <div class="fb-like" data-send="false" style='top: 0;' data-layout="button_count" data-width="55" data-show-faces="false" data-action="recommend"></div>

        <div style='width:94px;'>
            <a href="http://twitter.com/share" class="twitter-share-button" data-count="horizontal" data-lang="ru"  data-url="<?php echo Url::to($link, true);?>" data-text="<?php echo $title;?> via @_24_kg" data-lang="ru"></a>
        </div>
        <div style='width:72px;'>
            <g:plusone size="medium"></g:plusone>
        </div>

        <!-- Put this div tag to the place, where the Like block will be -->
        <div id="vk_like" style="height:23px;display: inline-block;"></div>


        <div class="fb-like" style="height:23px;display: inline-block;margin-right: 10px;" data-href="https://www.facebook.com/www.24.kg" data-layout="button_count" data-action="like" data-show-faces="false" data-share="false"></div>

        <a href="https://twitter.com/_24_kg" class="twitter-follow-button" data-show-count="false" data-lang="ru">Читать @_24_kg</a>
    </div>
</noindex>