<?php



/* @var $this yii\web\View */
/* @var $rates \yii2portal\currency\models\CurrencyVals[] */

$formatter = Yii::$app->formatter;

?>

<div class="block kurs_block">
    <h3>Учетный курс</h3>
    <div class="grey_zag"> с <?php echo $formatter->asDate($rates[0]->rate_date + $timeShift, "d MMMM"); ?></div>
    <table>
        <?php foreach ($rates as $rate): ?>
            <tr>
                <td class=kurs><?php echo mb_strtoupper($rate->name); ?></td>
                <td class=kurs2>
                    <span>
                        <?php if ($rate->nominal != 1): ?>
                            <?php echo $rate->rate / $rate->nominal; ?>
                        <?php else: ?>
                            <?php echo $rate->rate; ?>
                        <?php endif; ?>
                    </span>
                </td>
            </tr>
        <?php endforeach; ?>
    </table>
    <?php if ($avg): ?>
        <div class='zag'>Средневзвешенный курс сома на <?php echo $formatter->asDate($avg->time + $timeShift, "d MMMM"); ?></div>
        <table>
            <tr>
                <td class=kurs>USD</td>
                <td class=kurs2><?php echo $avg->rate ?></td>
            </tr>
        </table>
    <?php endif; ?>
    <p class="ta_l">Нацбанк КР</p>
</div>
