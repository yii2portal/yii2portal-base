<?php

use  \yii\helpers\Html;
use \yii\widgets\LinkPager;
use \yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $provider \yii\data\ActiveDataProvider */
/* @var $page \yii2portal\structure\models\CoreStructure */
/* @var $news \yii2portal\news\models\News[] */
/* @var $main \yii2portal\news\models\News */
/* @var $other_main \yii2portal\news\models\News[] */
/* @var $defaultMainImage \yii2portal\media\models\Media */

//$this->title = $page->title;

$openx = Yii::$app->getModule('openx');

$news = $provider->getModels();

$bundle = $this->assetManager->getBundle('frontend\themes\yii2portal\assets\AppAsset');

?>

<?php Pjax::begin([
    'timeout' => 5000,
    'id' => 'newslist'
]); ?>
<?php if ($main): ?>
    <div class="main_news clearfix">
        <div class="pic">


            <a data-pjax="0" href="<?php echo $new->urlPath ?>">
                <?php if ($main->imageMain): ?>
                    <?php
                    echo Html::img($main->imageMain->thumbnail(215, 161, $bundle->baseUrl . '/ima/nophoto.png'), [
                        'alt' => $main->title,
                        'class' => 'fl'
                    ]);
                    ?>
                <?php elseif ($defaultMainImage): ?>

                    <?php
                    echo Html::img($defaultMainImage->thumbnail(215, 161, $bundle->baseUrl . '/ima/nophoto.png'), [
                        'alt' => $main->title,
                        'class' => 'fl'
                    ]);
                    ?>
                <?php endif; ?>
            </a>


            <div></div>
        </div>
        <div class="text">
            <h2><a data-pjax="0" href="<?php echo $main->urlPath ?>"><?php echo $main->title ?></a></h2>
            <span class="date"><?php echo $main->dateTimeFormat("dd/MM/yyyy HH:mm"); ?></span>

            <div class="brief">


                <?php echo $main->newsDescription ?>


                <?php if ($main->hasPhoto): ?>
                    <a data-pjax="0" href="<?php echo $main->urlPath ?>" title="фото" class="has_photo">фото</a>
                <?php endif; ?>
                <?php if ($main->hasVideo): ?>
                    <a data-pjax="0" href="<?php echo $main->urlPath ?>" title="видео" class="has_video">видео</a>
                <?php endif; ?>
                <?php if ($main->hasInfo): ?>
                    <a data-pjax="0" href="<?php echo $main->urlPath ?>" title="инфографика" class="has_info">инфографика</a>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php endif; ?>
<?php if (count($other_main > 0)): ?>
    <div class="main_last_news">
        <?php foreach ($other_main as $i => $new): ?>
            <div class="one_news">
                <span class="date"><?php echo $new->dateTimeFormat("dd/MM/yyyy HH:mm"); ?></span>
                <div>
                    <p>
                        <a data-pjax="0" href="<?php echo $new->urlPath ?>"><?php echo $new->title ?></a>
                        <?php if ($new->hasPhoto): ?>
                            <a data-pjax="0" href="<?php echo $new->urlPath ?>" title="фото" class="has_photo">фото</a>
                        <?php endif; ?>
                        <?php if ($new->hasVideo): ?>
                            <a data-pjax="0" href="<?php echo $new->urlPath ?>" title="видео"
                               class="has_video">видео</a>
                        <?php endif; ?>
                        <?php if ($new->hasInfo): ?>
                            <a data-pjax="0" href="<?php echo $new->urlPath ?>" title="инфографика"
                               class="has_info">инфографика</a>
                        <?php endif; ?>
                    </p>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

<?php echo $openx->placeholder->insert(8); ?>
<div class="column_title" style="margin-top: 15px;">ЛЕНТА НОВОСТЕЙ <a href="http://eng.24.kg" rel="nofollow"
                                                                      class="fr"><img
            alt="English Version" src="<?php echo $bundle->baseUrl ?>/ima/news_brief.gif"
            style="margin-bottom: 2px;"/></a>
</div>
<div class="lenta_news">
    <?php foreach ($news as $i => $new): ?>
        <div class="one_news">
            <div class='clearfix'>
                <?php if ($new->imageLenta): ?>
                    <a data-pjax="0" href="<?php echo $new->urlPath ?>">
                        <?php
                        echo Html::img($new->imageLenta->thumbnail(80, 60, $bundle->baseUrl . '/ima/nophoto.png'), [
                            'alt' => $new->title,
                            'class' => 'fl'
                        ]);
                        ?>
                    </a>
                <?php endif; ?>

                <div>
                    <span class="date"><?php echo $new->dateTimeFormat("dd/MM/yyyy HH:mm"); ?></span>
                    <a data-pjax="0" href="<?php echo $new->urlPath ?>" class="n"><?php echo $new->title ?></a>
                    <?php if ($new->hasPhoto): ?>
                        <a data-pjax="0" href="<?php echo $new->urlPath ?>" title="фото" class="has_photo">фото</a>
                    <?php endif; ?>

                    <?php if ($new->hasVideo): ?>
                        <a data-pjax="0" href="<?php echo $new->urlPath ?>" title="видео" class="has_video">видео</a>
                    <?php endif; ?>

                    <?php if ($new->hasInfo): ?>
                        <a data-pjax="0" href="<?php echo $new->urlPath ?>" title="инфографика" class="has_info">инфографика</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>

        <?php if ($i == 2): ?><?php echo $openx->placeholder->insert(30); ?><?php endif; ?>
        <?php if ($i == 4): ?><?php echo $openx->placeholder->insert(9); ?><?php endif; ?>
        <?php if ($i == 9): ?><?php echo $openx->placeholder->insert(12); ?><?php endif; ?>
        <?php if ($i == 14): ?><?php echo $openx->placeholder->insert(13); ?><?php endif; ?>
        <?php if ($i == 19): ?><?php echo $openx->placeholder->insert(22); ?><?php endif; ?>
        <?php if ($i == 24): ?><?php echo $openx->placeholder->insert(23); ?><?php endif; ?>
    <?php endforeach; ?>
</div>

<?php
echo LinkPager::widget([
    'pagination' => $provider->pagination,
    'registerLinkTags' => !Yii::$app->request->isAjax,

]);
?>

<?php Pjax::end(); ?>
