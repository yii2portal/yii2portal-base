<?php

use \yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $name string */
/* @var $message string */
/* @var $exception Exception */

use yii\helpers\Html;

$this->title = $name;
?>
<div class="cont">

    <h1><?= Html::encode($this->title) ?></h1>

    <div class="alert alert-danger">
        <?= nl2br(Html::encode($message)) ?>
    </div>

    <ol>
        <li>Если вы вводили адрес вручную в адресной строке браузера, проверьте, все ли вы написали правильно;</li>
        <li>
            Если вы перешли по ссылке с другого сайта, попробуйте перейти на первую страницу <a href="/"><?php echo Yii::$app->request->getHostInfo()?></a>, вполне вероятно, там есть ссылка на нужный вам материал;
        </li>
        <li>
            А если уж вы встретили неработающую ссылку на страницах <a href="/"><?php echo Yii::$app->request->getHostInfo()?></a>, пожалуйста, <a href="mailto:webmaster@24.kg">напишите нам</a> об этом.
        </li>
    </ol>

</div>
