<?php


/* @var $this \yii\web\View */
/* @var $content string */
/* @var $blockNews \yii2portal\news\components\block\Block */
/* @var $structure \yii2portal\structure\Module */
/* @var $news \yii2portal\news\Module */
/* @var $menu \yii2portal\menu\Module */
/* @var $openx \yii2portal\openx\Module */

use \yii\helpers\Html;
use \yii\web\View;
use \yii\helpers\ArrayHelper;

use \yii2portal\media\models\Media;

use frontend\themes\yii2portal\assets\AppAsset;
use frontend\themes\yii2portal\assets\PrintAsset;
use \frontend\themes\yii2portal\assets\RunlineAsset;


$bundle = AppAsset::register($this);
$bundlePrint = PrintAsset::register($this);
RunlineAsset::register($this);


$structure = Yii::$app->getModule('structure');
$menu = Yii::$app->getModule('menu');
$sphinx = Yii::$app->getModule('sphinx');
$news = Yii::$app->getModule('news');
$openx = Yii::$app->getModule('openx');
$currency = Yii::$app->getModule('currency');
$weather = Yii::$app->getModule('weather');
$poll = Yii::$app->getModule('poll');
$archive = Yii::$app->getModule('archive');

$blockNews = $news->block;

$appParams = Yii::$app->params;
$siteName = Yii::$app->name;
$formatter = Yii::$app->formatter;

$cache = Yii::$app->cache;

//$datelasteuserdit = \yii2portal\news\models\News::find()->max("datelasteuserdit");

$newsDependency = Yii::createObject([
    'class' => 'yii2portal\news\components\CacheDependency'
]);
$newsCacheConfig = [
    'dependency' => $newsDependency,
    'duration' => 60 * 60
];


$isIndexPage = (empty(Yii::$app->requestedRoute) || Yii::$app->requestedRoute == Yii::$app->defaultRoute);
$index = $structure->getPageByModule('index');

$leftlimit = $index->getParamValue('leftlimit', 20);


$data = $cache->get($key);

if (false === ($leftNews = $cache->get('leftNews')) || false === ($topNews = $cache->get('topNews'))) {


    $leftNews = $blockNews->insert('left')
        ->with('imageCol')
        ->where([
            'is_afisha' => 1,
        ])
        ->andPublished()
        ->byDatepublic()
        ->orderBy(['left_datepublic' => SORT_DESC])
        ->limit($leftlimit)
        ->all();

    $topNews = $blockNews->insert('top')
        ->with('imageCol')
        ->where([
            'is_afisha' => 1,
        ])
        ->andFilterCompare('top_datepublic', time(), '>')
        ->andWhere(['not in', 'id', ArrayHelper::getColumn($leftNews->fetched(), 'id')])
        ->andPublished()
        ->byDatepublic()
        ->orderBy(['left_datepublic' => SORT_DESC])
        ->limit(100)
        ->all();





    /*$cache->multiSet([
        'leftNews' => $leftNews,
        'topNews' => $topNews,
    ], 60 * 60, $newsDependency);*/
}


?>
<?php $this->beginPage() ?>
    <!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <?= Html::csrfMetaTags() ?>

        <?php

        $mainPage = [
            'label' => $siteName,
            'url' => Yii::$app->homeUrl
        ];

        $this->params['breadcrumbs'] = isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [];
        foreach ($structure->currentPage->parents as $page) {
            $this->params['breadcrumbs'][] = ['label' => strip_tags($page->title), 'url' => $page->urlPath];
        }

        if (!$isIndexPage) {
            $this->params['breadcrumbs'][] = [
                'label' => strip_tags($structure->currentPage->title),
                'url' => $structure->currentPage->urlPath
            ];

        }

        $this->params['breadcrumbs'][] = $mainPage;

        $title = implode(" » ", ArrayHelper::getColumn($this->params['breadcrumbs'], 'label'));

        ?>
        <title><?php echo Html::encode($title); ?></title>

        <?php $this->registerLinkTag(['rel' => 'icon', 'type' => 'image/png', 'href' => $bundle->baseUrl . '/ima/favicon.ico']); ?>
        <?php $this->registerJsFile("http://html5shiv.googlecode.com/svn/trunk/html5.js", [
            'condition' => 'lt IE 9',
            'position' => View::POS_HEAD
        ]); ?>

        <?php $this->head() ?>

    </head>
    <body>
    <?php $this->beginBody() ?>


    <?php if (isset($this->blocks['branding_bg'])): ?>
        <?= $this->blocks['branding_bg'] ?>
    <?php endif; ?>


    <?php
    $js = <<<EOF
        window.fbAsyncInit = function () {
            FB.init({
                appId: '942470665830749',
                xfbml: true,
                version: 'v2.5'
            });
        };

        (function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) {
                return;
            }
            js = d.createElement(s);
            js.id = id;
            js.src = "//connect.facebook.net/ru_RU/sdk.js";
            fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
EOF;
    $this->registerJs($js, View::POS_END);
    ?>

    <div class="wrapper">
        <div class="noprint ta_c"><?php echo $openx->placeholder->insert(1); ?></div>
        <p class="logoprint"><img src="<?php echo $bundle->baseUrl ?>/ima/logo.png"/></p>


        <?php if (isset($this->blocks['branding_header'])): ?>
            <?= $this->blocks['branding_header'] ?>
        <?php else: ?>


            <header class="header">
                <div class="logo" id="logocont"></div>
                <?php
                $js = <<<EOF
                if (swfobject.hasFlashPlayerVersion("9.0.18")) {
                    var params = {
                        wmode: "transparent",
                        bgcolor: "#F9F9F9",
                        menu: false,
                        loop: false,
                        quality: "high"
                    };
                    swfobject.embedSWF("{$bundle->baseUrl}/ima/logo.swf", "logocont", "208", "126", "9", false, false, params);
                }
                else {
                    document.getElementById('logocont').innerHTML = '<a href="/"><img src="{$bundle->baseUrl}/ima/logo.png" alt="{$siteName}" title="{$siteName}"/></a>';
                }
EOF;
                $this->registerJs($js);
                ?>
                <div id="logobb"><?php echo $openx->placeholder->insert(2); ?></div>

                <?php echo $menu->placeholder->render('top') ?>

                <div class="top_icon">
                    <div>
                        <div class="rss"><a href="/rss/">RSS</a></div>
                        <div class="soc"><a target="_blank" href="https://www.facebook.com/www.24.kg"
                                            rel="nofollow"><img
                                    src="<?php echo $bundle->baseUrl ?>/ima/fb_icon.png"
                                    alt="facebook"/></a><a target="_blank"
                                                           href="https://twitter.com/_24_kg" rel="nofollow"><img
                                    src="<?php echo $bundle->baseUrl ?>/ima/tw_icon.png" alt="facebook"/></a><a
                                target="_blank"
                                href="https://plus.google.com/108133408319936775540"
                                rel="publisher"><img src="<?php echo $bundle->baseUrl ?>/ima/g.png"
                                                     alt="googale+"/></a>
                        </div>
                    </div>
                </div>

                <?php echo $topNews ?>
                <div class="lang"><a href="http://eng.24.kg/" rel="nofollow">ENG</a>|<a href="http://arch.24.kg/"
                                                                                        rel="nofollow">АРХИВЫ 24</a>
                </div>
                <div class="time">
                    Сегодня <?php echo $formatter->asDate("now", "d MMMM yyyy года. EEEE."); ?> <?php echo $appParams['timePhrase'] ?> <?php echo $formatter->asTime("now", "HH:mm"); ?>
                </div>
                <?php if ($sphinx): ?>
                    <?php echo $sphinx->widget->insert('informer'); ?>
                <?php endif; ?>
            </header><!-- .header-->

            <?php


            if ($this->beginCache('runline', $newsCacheConfig)) {
                echo $blockNews->insert('runline')
                    ->andModules(['news', 'news-perekrestok', 'news-vzglyad'])
                    ->andPublished()
                    ->byDatepublic()
                    ->orderBy(['datepublic' => SORT_DESC])
                    ->limit(20)
                    ->all();
                $this->endCache();
            }
            ?>

            <div class="mainMenu">
                <table cellpadding="0" cellspacing="0">
                    <tbody>
                    <?php echo $menu->placeholder->render('main') ?>
                    </tbody>
                </table>
            </div>

            <div class="daily_news"><b>НОВОСТИ ДНЯ:</b><span id="theTicker" style="font:13px arial;"></span></div>

        <?php endif; ?>

        <div class="noprint ta_c"><?php echo $openx->placeholder->insert([3, 4, 25]); ?></div>
        <div class="middle">
            <div class="container">
                <main class="content size1">
                    <div class="column_title">

                        <?php if ($isIndexPage): ?>
                            ГЛАВНЫЕ ТЕМЫ
                        <?php elseif ($structure->currentPage): ?>
                            <?php echo $structure->currentPage->title; ?>
                        <?php endif; ?>

                        <div class="fontsize">
                            <a title="уменьшить шрифт" class="sm" href="#"><span style="">A</span> [-]</a>
                            <a title="восстановить значение по умолчанию" href="#">размер текста</a>
                            <a title="увеличить шрифт" class="bg" href="#">[+] <span>A</span></a>
                        </div>
                    </div>

                    <?= $content ?>
                </main>
                <!-- .content -->
            </div>
            <!-- .container-->
            <aside class="left-sidebar">
                <div class="column_title"><a href="/">ГЛАВНАЯ</a></div>

                <?php echo $menu->placeholder->render('submenu') ?>


                <?php if ($isIndexPage): ?>

                    <?php foreach ($structure->getPagesByModule('structure-dop') as $dop): ?>

                        <?php if ($dop->getParamValue('is_vis') == 'on'): ?>
                            <div class="mb20">

                                <h3><?php echo $dop->title ?></h3>
                                <div class="block simple">
                                    <?php
                                    $image = Media::findOne($dop->getParamValue('image'));
                                    ?>

                                    <?php if ($image): ?>
                                        <a href="<?php echo $new->urlPath ?>">
                                            <?php
                                            echo Html::img($image->thumbnail(152, 114, $bundle->baseUrl . '/ima/nophoto.png'), [
                                                'alt' => $dop->title
                                            ]);
                                            ?>
                                        </a>
                                    <?php endif; ?>

                                    <a class="title"
                                       href="<?php echo $dop->urlPath ?>"><?php echo $dop->getParamValue('descr'); ?></a>
                                </div>
                            </div>
                        <?php endif; ?>
                    <?php endforeach; ?>
                <?php endif; ?>


                <?php

                $pids = array();
                foreach ($structure->getPagesByModule('news-press') as $s) {
                    if ($s->getParamValue('is_vis') == 'on') {
                        $pids[] = $s->id;
                    }
                }
                if (!empty($pids)) {
                    if ($this->beginCache('press', $newsCacheConfig)) {
                        echo $blockNews->insert('press')
                            ->with('imageCol')
                            ->where(['pid' => $pids])
                            ->andPublished()
                            ->byDatepublic()
                            ->andFilterCompare('press_time', time(), '>')
                            ->orderBy(['datepublic' => SORT_ASC])
                            ->limit($index->getParamValue('presslimit', 1))
                            ->all();
                        $this->endCache();
                    }
                }
                ?>

                <div class="mb20">
                    <?php echo $openx->placeholder->insert(5); ?>
                    <?php echo $openx->placeholder->insert(34); ?>
                </div>

                <div class="mb20"><?php echo $openx->placeholder->insert(33); ?></div>

                <hr style="border: none;border-top: solid 1px #c0c0d8;margin:-15px 0 15px 0;"/>

                <?php echo $archive->calendar->insert('informer') ?>

                <?php echo $leftNews; ?>

                <?php if ($isIndexPage): ?>
                    <?php echo $openx->placeholder->insert(6); ?>
                <?php endif; ?>
            </aside>
            <!-- .left-sidebar -->


            <?php if (isset($this->blocks['branding_sideblock'])): ?>
                <?= $this->blocks['branding_sideblock'] ?>
            <?php else: ?>
                <aside class="right-sidebar">
                    <?php if ($isIndexPage): ?>
                        <?php echo $openx->placeholder->insert(7); ?>
                    <?php else: ?>
                        <?php echo $openx->placeholder->insert([17, 18, 19]); ?>
                    <?php endif; ?>

                    <div class="column_title">&nbsp;</div>

                    <?php
                    if ($this->beginCache('currency', ['duration' => 60 * 60 * 6])) {
                        echo $currency->informer->insert('informer');
                        $this->endCache();
                    }
                    ?>

                    <div class="mb20"><?php echo $openx->placeholder->insert(10); ?></div>

                    <?php
                    if ($this->beginCache('weather', ['duration' => 60 * 60 * 6])) {
                        echo $weather->informer->insert('informer');
                        $this->endCache();
                    }
                    ?>

                    <div class="mb20"><?php echo $openx->placeholder->insert(15); ?></div>

                    <?php
                    if ($this->beginCache('vzglyad', $newsCacheConfig)) {
                        echo $blockNews->insert('vzglyad')
                            ->with('imageCol')
                            ->andModules(['news-vzglyad'])
                            ->andPublished()
                            ->byDatepublic()
                            ->orderBy(['datepublic' => SORT_DESC])
                            ->limit($index->getParamValue('vzglyadlimit', 1))
                            ->all();
                        $this->endCache();
                    }
                    ?>

                    <?php echo $openx->placeholder->insert(16); ?>

                    <?php
                    if ($this->beginCache('perekrestok', $newsCacheConfig)) {
                        echo $blockNews->insert('perekrestok')
                            ->with('imageCol')
                            ->andModules(['news-perekrestok'])
                            ->andPublished()
                            ->byDatepublic()
                            ->orderBy(['datepublic' => SORT_DESC])
                            ->limit($index->getParamValue('perekrlimit', 1))
                            ->all();
                        $this->endCache();
                    }
                    ?>


                    <div class="mb20"></div>
                    <?php echo $poll->widget->insert('informer'); ?>

                    <div class="news_column_list">

                        <?php
                        if ($this->beginCache('doslovno', $newsCacheConfig)) {
                            echo $blockNews->insert('doslovno')
                                ->with('imageCol')
                                ->andModules(['news-doslovno'])
                                ->andPublished()
                                ->byDatepublic()
                                ->orderBy(['datepublic' => SORT_DESC])
                                ->limit($index->getParamValue('doslovnolimit', 2))
                                ->all();
                            $this->endCache();
                        }
                        ?>
                        <?php
                        if ($this->beginCache('tiraj', $newsCacheConfig)) {
                            echo $blockNews->insert('tiraj')
                                ->with('imageCol')
                                ->andModules(['news-tiraj'])
                                ->andPublished()
                                ->byDatepublic()
                                ->orderBy(['datepublic' => SORT_DESC])
                                ->limit($index->getParamValue('tirajlimit', 10))
                                ->all();
                            $this->endCache();
                        }
                        ?>

                    </div>
                    <div class="counters">
                        <noindex>
                            <!-- Begin of RAMBER -->
                            <!--begin of Rambler's Top100 code -->
                            <a href="http://top100.rambler.ru/top100/"> <img
                                    src="http://counter.rambler.ru/top100.cnt?899430"
                                    alt="" width=1 height=1 border=0></a>
                            <!--end of Top100 code-->
                            <!--begin of Top100 logo-->
                            <br/>
                            <a href="http://top100.rambler.ru/top100/"><img
                                    src="http://top100-images.rambler.ru/top100/banner-88x31-rambler-gray2.gif"
                                    alt="Rambler's Top100" width=88 height=31 border=0></a>
                            <!--end of Top100 logo -->
                            <!-- End of RAMBLER -->
                            <br/>
                            <!--Rating@Mail.ru COUNTEr-->
                            <script language="JavaScript" type="text/javascript">
                                d = document;
                                var a = '';
                                a += ';r=' + escape(d.referrer);
                                js = 10;
                            </script>
                            <script language="JavaScript1.1" type="text/javascript">
                                a += ';j=' + navigator.javaEnabled();
                                js = 11;
                            </script>
                            <script language="JavaScript1.2" type="text/javascript">
                                s = screen;
                                a += ';s=' + s.width + '*' + s.height;
                                a += ';d=' + (s.colorDepth ? s.colorDepth : s.pixelDepth);
                                js = 12;
                            </script>
                            <script language="JavaScript1.3" type="text/javascript">
                                js = 13;
                            </script>
                            <script language="JavaScript" type="text/javascript">
                                d.write('<a href="http://top.mail.ru/jump?from=1048096"' +
                                    ' target=_top><img src="http://de.cf.bf.a0.top.list.ru/counter' +
                                    '?id=1048096;t=48;js=' + js + a + ';rand=' + Math.random() +
                                    '" alt="Рейтинг@Mail.ru"' + ' border=0 height=31 width=88/></a>');
                                if (11 < js) {
                                    d.write('<' + '!-- ');
                                }
                            </script>
                            <noscript>
                                <a
                                    target=_top href="http://top.mail.ru/jump?from=1048096"><img
                                        src="http://de.cf.bf.a0.top.list.ru/counter?js=na;id=1048096;t=48"
                                        border=0 height=31 width=88
                                        alt="тЕКФЙОЗ@Mail.ru"/></a>
                            </noscript>
                            <script language="JavaScript" type="text/javascript">
                                if (11 < js) {
                                    d.write('--' + '>');
                                }
                            </script>
                            <!--/COUNTER-->
                            <br/>
                            <!--LiveInternet counter-->
                            <script type="text/javascript">
                                document.write("<a href='http://www.liveinternet.ru/click' " +
                                    "target=_blank><img src='http://counter.yadro.ru/hit?t29.2;r" +
                                    escape(document.referrer) + ((typeof (screen) == "undefined") ? "" :
                                    ";s" + screen.width + "*" + screen.height + "*" + (screen.colorDepth ?
                                        screen.colorDepth : screen.pixelDepth)) + ";u" + escape(document.URL) +
                                    ";h" + escape(document.title.substring(0, 80)) + ";" + Math.random() +
                                    "' alt='' title='LiveInternet: показано количество просмотров и" +
                                    " посетителей' " +
                                    "border='0' width='88' height='120'></a>");
                            </script>
                            <!--/LiveInternet--><br>
                            <a href="http://yandex.ru/cy?base=0&amp;host=24kg.org"><img
                                    src="http://www.yandex.ru/cycounter?24kg.org" width="88" height="31"
                                    alt="/=45:A F8B8@>20=8O" border="0"/></a><br>
                            <!-- WWW.NET.KG , code for http://24.kg -->
                            <script language="javascript" type="text/javascript">
                                java = "1.0";
                                java1 = "" + "refer=" + escape(document.referrer) + "&amp;page=" + escape(window.location.href);
                                document.cookie = "astratop=1; path=/";
                                java1 += "&amp;c=" + (document.cookie ? "yes" : "now");
                            </script>
                            <script language="javascript1.1" type="text/javascript">
                                java = "1.1";
                                java1 += "&amp;java=" + (navigator.javaEnabled() ? "yes" : "now");
                            </script>
                            <script language="javascript1.2" type="text/javascript">
                                java = "1.2";
                                java1 += "&amp;razresh=" + screen.width + 'x' + screen.height + "&amp;cvet=" +
                                    (((navigator.appName.substring(0, 3) == "Mic")) ?
                                        screen.colorDepth : screen.pixelDepth);
                            </script>
                            <script language="javascript1.3" type="text/javascript">java = "1.3"</script>
                            <script language="javascript" type="text/javascript">
                                java1 += "&amp;jscript=" + java + "&amp;rand=" + Math.random();
                                document.write("<a href='http://www.net.kg/stat.php?id=1839&amp;fromsite=1839' target='_blank'>" +
                                    "<img src='http://www.net.kg/img.php?id=1839&amp;" + java1 +
                                    "' border='0' alt='WWW.NET.KG' width='0' height='0' /></a>");
                            </script>
                            <noscript>
                                <a href='http://www.net.kg/stat.php?id=1839&amp;fromsite=1839' target='_blank'><img
                                        src="http://www.net.kg/img.php?id=1839" border='0' alt='WWW.NET.KG' width='0'
                                        height='0'/></a>
                            </noscript>
                        </noindex>
                        <!-- /WWW.NET.KG -->

                        <!-- Yandex.Metrika informer -->
                        <a href="https://metrika.yandex.ru/stat/?id=22715299&amp;from=informer"
                           target="_blank" rel="nofollow"><img
                                src="//bs.yandex.ru/informer/22715299/3_0_FFFFFCFF_F5F5DCFF_0_pageviews"
                                style="width:88px; height:31px; border:0;" alt="Яндекс.Метрика"
                                title="Яндекс.Метрика: данные за сегодня (просмотры, визиты и уникальные посетители)"
                                onclick="try{Ya.Metrika.informer({i:this,id:22715299,lang:'ru'});return false}catch(e){}"/></a>
                        <!-- /Yandex.Metrika informer -->

                        <!-- Yandex.Metrika counter -->
                        <script type="text/javascript">
                            (function (d, w, c) {
                                (w[c] = w[c] || []).push(function () {
                                    try {
                                        w.yaCounter22715299 = new Ya.Metrika({
                                            id: 22715299,
                                            webvisor: true,
                                            clickmap: true,
                                            trackLinks: true,
                                            accurateTrackBounce: true
                                        });
                                    } catch (e) {
                                    }
                                });

                                var n = d.getElementsByTagName("script")[0],
                                    s = d.createElement("script"),
                                    f = function () {
                                        n.parentNode.insertBefore(s, n);
                                    };
                                s.type = "text/javascript";
                                s.async = true;
                                s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

                                if (w.opera == "[object Opera]") {
                                    d.addEventListener("DOMContentLoaded", f, false);
                                } else {
                                    f();
                                }
                            })(document, window, "yandex_metrika_callbacks");
                        </script>
                        <noscript>
                            <div><img src="//mc.yandex.ru/watch/22715299" style="position:absolute; left:-9999px;"
                                      alt=""/>
                            </div>
                        </noscript>
                        <!-- /Yandex.Metrika counter -->
                    </div>

                </aside>


                <!-- .right-sidebar -->
            <?php endif; ?>
        </div>
    </div>
    <!-- .wrapper -->

    <?php if (isset($this->blocks['branding_footer'])): ?>
        <?= $this->blocks['branding_footer'] ?>
    <?php else: ?>
        <footer class="footer">
            © ИА "24.kg". Все права защищены. Вся информация, размещённая на данном веб-сайте, предназначена только для
            персонального использования и не подлежит распространению без письменного разрешения ИА "24.kg"
        </footer>
    <?php endif; ?>


    <?php $this->endBody() ?>
    </body>
    </html>
<?php $this->endPage() ?>