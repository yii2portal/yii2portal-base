<?php
namespace frontend\controllers;

use Yii;
use yii\base\InlineAction;
use yii\data\ActiveDataProvider;

use yii\helpers\ArrayHelper;
use yii2portal\media\models\Media;
use yii2portal\news\controllers\IndexController as NewsIndexController;
use yii2portal\news\models\News;

/**
 * Site controller
 */
class NewsController extends NewsIndexController
{
    /*public function behaviors()
    {
        return [
            [
                'class' => 'yii\filters\HttpCache',
                'only' => ['view'],
                'lastModified' => function (InlineAction $action, $params) {
                    return $lastEditedGlobal;
                },
                'etagSeed' => function ($action, $params) {
                    return serialize([$lastEditedGlobal]);
                },
            ],
        ];
    }*/


    public function actionIndex($structure_id)
    {

        $indexPage = Yii::$app->getModule('structure')->getPageByModule('index');

        $mainLimit = 4;
        $lentaLimit = 50;
        $defaultMainImage = null;

        $lentaLimit = $indexPage->getParamValue('catlimit', 50);
        if ($indexPage) {
            $mainLimit = $indexPage->getParamValue('mainlimit');
            $lentaLimit = $indexPage->getParamValue('mainlentalimit');
            $defaultMainImage = Media::findOne($indexPage->getParamValue('main_default_image'));
        }

        $mainNews = News::find()
            ->with('imageMain')
            ->andWhere([
                'pid' => $structure_id,
                'is_main_cat' => 1
            ])
            ->andPublished()
            ->byDatepublic()
            ->orderBy(['datepublic' => SORT_DESC])
            ->limit($mainLimit)
            ->all();


        $provider = new ActiveDataProvider([
            'query' => News::find()
                ->with('imageLenta', 'newsAgency', 'newsAgency.imageLogo')
                ->andWhere([
                    'pid' => $structure_id
                ])
                ->andWhere([
                    'not in', 'id', ArrayHelper::getColumn($mainNews, 'id')
                ])
                ->andPublished()
                ->orderBy(['datepublic' => SORT_DESC])
                ->byDatepublic(),
            'pagination' => [
                'pageSize' => $lentaLimit,
                'defaultPageSize' => $lentaLimit
            ],
            'sort' => [
                'defaultOrder' => [
                    'datepublic' => SORT_DESC
                ]
            ],
        ]);

        $page = Yii::$app->modules['structure']->getPage($structure_id);
        $view = $page->module == 'news-tiraj' ? 'tiraj' : 'index';

        return $this->render($view, [
            'page' => $page,
            'provider' => $provider,
            'main' => !empty($mainNews) ? array_shift($mainNews) : null,
            'other_main' => $mainNews,
            'defaultMainImage' => $defaultMainImage
        ]);
    }


    public function actionView($structure_id, $item_id)
    {


        $new = News::find()
            ->with('baseImage')
            ->andWhere([
                'id' => $item_id
            ])
            ->andPublished()
            ->byDatepublic()
            ->one();
        if (!$new) {
            throw new NotFoundHttpException();
        } elseif ($new->pid != $structure_id || !Yii::$app->request->get('translit')) {
            return $this->redirect($new->urlPath, 301);
        }

        $page = Yii::$app->modules['structure']->getPage($structure_id);
        $view = "view";
        switch ($page->module) {
            case "news-tiraj":
                $view = "view-tiraj";
                break;
            case "news-press":
                $view = "view-press";
                break;
        }

        return $this->render($view, [
            'category' => Yii::$app->modules['structure']->getPage($structure_id),
            'new' => $new
        ]);
    }

}
