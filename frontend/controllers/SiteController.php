<?php
namespace frontend\controllers;

use Yii;
use yii\data\ActiveDataProvider;
use yii\web\Controller;

use yii2portal\media\models\Media;
use yii2portal\news\components\CacheDependency;
use yii2portal\news\models\News;

/**
 * Site controller
 */
class SiteController extends Controller
{

    public function behaviors()
    {
        $lastEditedGlobal = Yii::$app->getModule('news')->lastEditedGlobal;
        return [
            [
                'class' => 'yii\filters\HttpCache',
                'only' => ['index'],
                'lastModified' => function ($action, $params) use ($lastEditedGlobal) {
                    return $lastEditedGlobal;
                },
                'etagSeed' => function ($action, $params) use ($lastEditedGlobal) {
                    return serialize([$lastEditedGlobal]);
                },
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ]
        ];
    }

    /**
     * Displays homepage.
     *
     * @return mixed
     */
    public function actionIndex()
    {
        $mainLimit = 4;
        $lentaLimit = 50;
        $defaultMainImage = null;
        $indexPage = Yii::$app->getModule('structure')->getPageByModule('index');
        $dependency = new CacheDependency();

        if ($indexPage) {
            $mainLimit = $indexPage->getParamValue('mainlimit');
            $lentaLimit = $indexPage->getParamValue('mainlentalimit');
            $defaultMainImage = Media::findOne($indexPage->getParamValue('main_default_image'));
        }


        $mainNews = News::getDb()->cache(function ($db) use ($mainLimit) {
            return News::find()
                ->with('imageMain')
                ->andMain()
                ->andPublished()
                ->byDatepublic()
                ->orderBy(['datepublic' => SORT_DESC])
                ->limit($mainLimit)
                ->all();
        },3600, $dependency);

        $provider = new ActiveDataProvider([
            'query' => News::find()
                ->with('imageLenta')
                ->andModules(['news', 'news-perekrestok', 'news-vzglyad'])
                ->andPublished()
                ->orderBy(['datepublic' => SORT_DESC])
                ->byDatepublic(),
            'pagination' => [
                'pageSize' => $lentaLimit,
                'defaultPageSize' => $lentaLimit
            ],
            'sort' => [
                'defaultOrder' => [
                    'datepublic' => SORT_DESC
                ]
            ],
        ]);


        News::getDb()->cache(function ($db) use ($provider) {
            $provider->prepare();
        }, 3600, $dependency);

        return $this->render('index', [
            'provider' => $provider,
            'main' => array_shift($mainNews),
            'other_main' => $mainNews,
            'defaultMainImage' => $defaultMainImage
        ]);
    }


}
