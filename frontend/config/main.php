<?php
$params = array_merge(
    require(__DIR__ . '/../../common/config/params.php'),
    require(__DIR__ . '/../../common/config/params-local.php'),
    require(__DIR__ . '/params.php'),
    require(__DIR__ . '/params-local.php')
);

$modules = [];
$dirPath = __DIR__ . "/modules";
$files = scandir($dirPath);
foreach ($files as $file) {
    if (is_file("{$dirPath}/{$file}")) {
        $moduleName = str_replace(".php", '', $file);
        $modules[$moduleName] = include("{$dirPath}/{$file}");
    }
}


return [
    'id' => 'app-frontend',
    'name' => 'www.24.kg - КЫРГЫЗСТАН',
    'basePath' => dirname(__DIR__),
    'layout' => 'main',
    'defaultRoute' => 'site/index',
    'language' => 'ru-RU',
    'bootstrap' => [
//        'log',
        'structure',
        'minify',
        'ga',
        'cplugin',
    ],
    'controllerNamespace' => 'frontend\controllers',
    'modules' => $modules,

    'aliases' => [
        '@themeName' => 'yii2portal',
        '@themePath' => '@app/themes/yii2portal'
    ],

    'components' => [

        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'view' => [
            'theme' => [
                'baseUrl' => '@web/themes/@themeName',
                'pathMap' => [
                    '@app/views' => '@themePath/views',
                ],
            ],
        ],
        'assetManager' => [
            'appendTimestamp' => true,
            'linkAssets' => true,
            /*'bundles' => [
                'yii\bootstrap\BootstrapAsset' => [
                    'css' => [],
                ],
                'backend\assets\VisAsset' => [
                    'css' => [],
                ]
            ]*/

        ],
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'suffix' => '/',
            'rules'=>[
                [
                    'pattern' => "",
                    'route' => "site/index",
                    'defaults' => [
                        'page' => 1
                    ]
                ],
                [
                    'pattern' => "page_<page:\d+>",
                    'route' => "site/index",
                    'defaults' => [
                        'page' => 1
                    ]
                ],
            ]
        ],
    ],
    'params' => $params,
];
