<?php


return [
    'class'=>'yii2portal\news\Module',
    'viewPath' => '@themePath/modules/news',
    'controllerMap' => [
        'index' => 'frontend\controllers\NewsController'
    ],
];