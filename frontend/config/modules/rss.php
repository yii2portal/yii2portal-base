<?php


return [
    'class'=>'yii2portal\rss\Module',
    'assetClassName'=>\frontend\themes\yii2portal\assets\AppAsset::className(),
    'miniLogo'=>'/ima/mini_logo.png',
    'squareLogo'=>'/ima/square_logo.png'
];