<?php


return [
    'class' => 'yii2portal\menu\Module',
    'positionsConfig' => [
        'top' => [
            'options' => ['tag' => 'nav'],
            'itemOptions' => ['tag' => false],
            'linkTemplate' => '<a href="{url}">{label}</a>',
        ],
        'main' => [
            'options' => ['tag' => 'tr'],
            'itemOptions' => ['tag' => 'td'],
            'linkTemplate' => '<a href="{url}" rel="tag">{label}</a>',
        ],
        'submenu' => [
            'options' => ['class' => 'category_list block'],
            'linkTemplate' => '<a href="{url}" rel="tag">{label}</a>',
        ],
    ]
];