<?php
return [
    'timePhrase' => 'Бишкекское время',
    'adminEmail' => 'admin@example.com',
    'media'=>[
        'path'=>'@app/../files/media',
        'src'=>'@web/files/media',
        'thumbnail'=>[
            'path'=>'@runtime/thumbnails',
            'url'=>'/thumbnails',
        ]
    ]
];
